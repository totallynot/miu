module.exports = function() {
	this.btwn = function(min,max) {
		return Math.floor(
			Math.random() * (max - min + 1) + min
		);
	};
	this.newUsr = function(a,b) {
		const usrSetup = {credit: b, bank: 0, job: "", items: ["test1", "test2"]};
		const usrSetupString = JSON.stringify(usrSetup);
		const fs = require('fs');
		fs.writeFile(`usr/${a}`, usrSetupString, function(err, result) {
			if(err) console.log('error', err);
		});
	};
	this.chgCredit = function(a,b) {
		const fs = require('fs');
                const data = fs.readFileSync(`usr/${a}`, 'utf8');
		const usrInv = JSON.parse(data);
		usrInv.credit = usrInv.credit + b;
		const usrInvString = JSON.stringify(usrInv);
		fs.writeFile(`usr/${a}`, usrInvString, function(err, result) {
                        if(err) console.log('error', err);
                });
        };
    this.multiply = function(a,b) { return a*b };
    //etc
}
