const { EmbedBuilder } = require('discord.js');
const { SlashCommandBuilder } = require('discord.js');
const fs = require('fs');
require('../../data/utl/functions.js')();

module.exports = {
	data: new SlashCommandBuilder()
		.setName('beg')
		.setDescription('Beg for Items or Credits.'),
	async execute(interaction) {
		await interaction.deferReply();
		const begEmbed = {color: 0x0099FF,title: 'You Begged!'};
		let begCredit = btwn(-15, 120);
		if (begCredit < 0) {
			begEmbed.description = `Unholy Curse! You just got hit by Taxes! **${begCredit}** credits!`;
		} else if (begCredit == 0) {
			begEmbed.description = 'You got **THE AMOUNT OF DIGNITY YOU HAVE**!';
		} else if (begCredit >=100) {
			begEmbed.description = `Holy Grace! You got blessed with **${begCredit}** credits!`;
		} else {
			begEmbed.description = `You got **${begCredit}** credits!`;
		};
		await interaction.editReply({ embeds: [begEmbed] });
		const usrFile = `usr/${interaction.user.id}`;
		fs.access(usrFile, fs.F_OK, (err) => {
			if (err) {
				console.error(err);
				newUsr(interaction.user.id,begCredit);
				return
			};

			chgCredit(interaction.user.id,begCredit);
		});
	},
};
