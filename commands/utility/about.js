const { EmbedBuilder } = require('discord.js');
const { SlashCommandBuilder } = require('discord.js');

module.exports = {
	cooldown: 5,
	data: new SlashCommandBuilder()
		.setName('about')
		.setDescription('Information About The App'),
	async execute(interaction) {
		await interaction.deferReply();
		const abtEmbed = new EmbedBuilder()
			.setColor(0x0099FF)
			.setTitle('Princess Miu')
			.setURL('https://gitlab.com/totallynot/miu')
			.setAuthor({ name: 'TotallyNot',  url: 'https://gitlab.com/totallynot' })
			.setDescription('Miu is a Big and Strong Princess of a Big and Stong Kingdom. She is here to Test your Server Members in Fun and Exciting Ways.')
			.setThumbnail('https://cdn.discordapp.com/avatars/1247152561648046092/43bad998346f54d07b0df8c4f32152ad.webp');

		await interaction.editReply({ embeds: [abtEmbed] });
	},
};
